package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.*;
import play.libs.F.*;
import play.libs.Json;
import play.libs.ws.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render());
    }

    public static Result oldindex() {
        return ok(oldindex.render());
    }

    public static Promise<Result> commitsCount(String repoOwner,
                                               String repoName,
                                               String startDateTime,
                                               String stopDateTime) {
        String url = "http://localhost:9001/commits_count";
        WSRequestHolder request =
                WS.url(url)
                  .setQueryParameter("repo_owner", repoOwner)
                  .setQueryParameter("repo_name", repoName)
                  .setQueryParameter("start_date", startDateTime)
                  .setQueryParameter("stop_date", stopDateTime);

        return request.get()
                .map(response -> (Result) ok(response.getBody()))
                .recover(ex -> internalServerError(Json.newObject().put("error", ex.getMessage())));
    }

    public static Promise<Result> contributors(String repoOwner, String repoName) {
        String url = "http://localhost:9001/contributors";
        WSRequestHolder request =
                WS.url(url)
                        .setQueryParameter("repo_owner", repoOwner)
                        .setQueryParameter("repo_name", repoName);

        return request.get()
                .map(response -> (Result) ok(response.getBody()))
                .recover(ex -> internalServerError(Json.newObject().put("error", ex.getMessage())));
    }
}
