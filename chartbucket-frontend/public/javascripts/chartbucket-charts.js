
Chart.defaults.global.responsive = true;

function generateContributorsChart (element_id, contributors) {
    var canvas = document.getElementById(element_id),
        ctx = canvas.getContext('2d');
    var colours = ["#0A528D", "#b9525c", "#256237"];

    var moduleData = {results: []};
    for (var i=0; i<contributors.length; i++) {
        moduleData.results.push({value: contributors[i].count,
            color: colours[i%3],
            highlight: Colour(colours[i%3], 10),
            label: contributors[i].name});
    }

    var moduleDoughnut = new Chart(ctx).Doughnut(moduleData.results,
        {responsive: true,
            animation: true,
            onAnimationComplete: function(){
                this.options.animation = true;
            }});
    moduleDoughnut.update();
}