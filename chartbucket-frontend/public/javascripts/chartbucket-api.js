//GET commits count
function getCommitsCount(repo_owner, repo_name, start_date, stop_date, callback) {
    $.ajax({
        url: '/commits_count',
        type: 'get',
        data: { repo_owner: repo_owner, repo_name: repo_name, start_date: start_date,  stop_date: stop_date},
        dataType: 'json',
        success: function(result, status, xhr) {
            //FIXME: необходим автоматический редирект на error function
            if (!result.error) {
                callback(result.count);
                console.log('ajax get commits_count success: '+ result +' '+ status +' '+ xhr);}
        },
        error: function(xhr, status, error) {
            console.log('ajax  get commits_count error: '+ xhr +' '+ status +' '+ error);
        }});
}

//GET contributors
function getContributors(repo_owner, repo_name, callback) {
    $.ajax({
        url: '/contributors',
        type: 'get',
        data: { repo_owner: repo_owner, repo_name: repo_name},
        dataType: 'json',
        success: function(result, status, xhr) {
            //FIXME: необходим автоматический редирект на error function
            if (!result.error) {
                callback(result);
                console.log('ajax get contributors success: '+ result +' '+ status +' '+ xhr);}
        },
        error: function(xhr, status, error) {
            console.log('ajax get contributors error: '+ xhr +' '+ status +' '+ error);
        }});
}