
START_DATE_MILLISECONDS = 86400000;

function getDateJSONISO8601(milliseconds) {
    if (arguments.length == 0) {
        var d = new Date();
    } else {
        var d = new Date(milliseconds);
    }
    var n = d.toJSON();
    return n;
}