var FIRST_HTML = "<div class='wrapper-inner-container' id='page-first'>" +
                    "<div class='cover-container'>" +
                        "<h1 class='chartbucket-first' id='id_chartbucket-first'></h1>" +
                    "</div>" +
                 "</div>";

var FIRST_FA_HTML = "<i class='fa fa-dot-circle-o'></i> ";

var SECOND_HTML = "<div class='wrapper-inner-container' id='page-second'>" +
                    "<div class='cover-container' id='id_container-second'>" +
                    "</div>" +
                  "</div>";

var SECOND_CANVAS_HTML = "<canvas id='id_chartbucket-canvas-second'" +
                                 "style='width: 1000px; height: 1000px; cursor: default;'>" +
                         "</canvas>";