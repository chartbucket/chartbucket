var REPO_OWNER_AVATAR = "assets/images/repo-owner-avatar.png";

function getOwnerInformation (term) {
    var repo_owner, repo_name;
    var tagList = term.split('/');
    var l = tagList.length;
    switch (l) {
        case 1:
            repo_owner = tagList[0];
            repo_name = "";
            break;
        case 2:
            repo_owner = tagList[0];
            repo_name = tagList[1];
            break;
        default:
            repo_owner = tagList[l-1-1];
            repo_name = tagList[l-1];
            break;
    }
    return {repo_owner: repo_owner,
        repo_name: repo_name};
}


$(document).ready(function() {
    $("#s2id_generateCharts").select2({
        placeholder: "Search for a repository",
        minimumInputLength: 3,
        query: function (query) {
            var data = {results: []};
            var owner_inf = getOwnerInformation(query.term);
            var repo_owner = owner_inf.repo_owner;
            var repo_name = owner_inf.repo_name;
            var full_name = repo_owner + "/" + repo_name;

            //FIXME: Избыточность
            data.results.push({id: {repo_owner: repo_owner,
                                    repo_name: repo_name},
                                    full_name: full_name,
                                    repo_owner: repo_owner,
                                    repo_name: repo_name,
                                    owner: {avatar_url: REPO_OWNER_AVATAR},
                                    forks_count: 0,
                                    stargazers_count: 0,
                                    watchers_count: 0});
            query.callback(data);
        },
        formatResult: repoFormatResult,
        formatSelection: repoFormatSelection,
        dropdownCssClass: "bigdrop",
        escapeMarkup: function (m) { return m; }
    });
});

function getCommitsCountCallback(count) {
    //FIXME: проверка на присутствие wrapper-container в списке классов
    var d = document.getElementById("id_first");
    //d.className = d.className + " wrapper-container";
    d.className = "wrapper-container";
    d.innerHTML = FIRST_HTML;
    document.getElementById("id_chartbucket-first").innerHTML = FIRST_FA_HTML + count;

    //FIXME: проверка на присутствие active в списке классов
    var l = document.getElementById("id_li-first");
    l.className = l.className + "active";

    location.href='/#page-first';
}

function getContributorsCallback(contributors) {
    //FIXME: проверка на присутствие wrapper-container в списке классов
    var d = document.getElementById("id_second");
    //d.className = d.className + " wrapper-container";
    d.className = "wrapper-container";
    d.innerHTML = SECOND_HTML;

    document.getElementById("id_container-second").innerHTML = SECOND_CANVAS_HTML;

    generateContributorsChart('id_chartbucket-canvas-second', contributors);
}

function generateStatistics() {
    var owner_inf = $("#s2id_generateCharts").select2("val");
    var repo_name = owner_inf.repo_name;
    var repo_owner = owner_inf.repo_owner;

    if(repo_name && repo_owner) {
        console.log("index.js -> generateCharts(): repo_owner - ",repo_owner, "; repo_name - ", repo_name);
        getCommitsCount(repo_owner, repo_name, getDateJSONISO8601(START_DATE_MILLISECONDS), getDateJSONISO8601(), getCommitsCountCallback);
        getContributors(repo_owner, repo_name, getContributorsCallback);
    }
}