package controllers;

import play.*;
import play.libs.Json;
import play.libs.oauth.OAuth.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {
    public static Result index() {
        RequestToken token = getSessionTokenPair();

        if (token == null) {
            return redirect("http://localhost:9000/bitbucket_auth?redirect_url=http://localhost:9001/");
        }
        return ok(Json.toJson(getSessionTokenPair()));
    }

    private static RequestToken getSessionTokenPair() {
        if (session().containsKey("token")) {
            return new RequestToken(session("token"), session("secret"));
        }
        else {
            return null;
        }
    }
}
