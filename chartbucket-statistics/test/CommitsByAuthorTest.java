import data_structures.Author;
import data_structures.Commit;
import data_structures.Repository;
import org.junit.Test;
import statistics_providers.CommitsByAuthorCalculator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommitsByAuthorTest {
    @Test
    public void positiveTest() {
        List<Commit> commitList = new ArrayList<>();

        commitList.add(new Commit(
                new Author("test1", "test1user"), new Repository("test_repo"),
                LocalDateTime.now(), "test message 1"));

        commitList.add(new Commit(
                new Author("test1", "test1user"), new Repository("test_repo"),
                LocalDateTime.now(), "test message 3"));

        commitList.add(new Commit(
                new Author("test1", "test1user"), new Repository("test_repo"),
                LocalDateTime.now(), "test message 4"));

        commitList.add(new Commit(
                new Author("test2", "test2user"), new Repository("test_repo"),
                LocalDateTime.now(), "test message 2"));

        CommitsByAuthorCalculator calc = new CommitsByAuthorCalculator();
        Map<String, Integer> res = calc.calculate(commitList);

        assert  res.containsKey("test1") && res.get("test1") == 3 &&
                res.containsKey("test2") && res.get("test2") == 1;
    }
}
