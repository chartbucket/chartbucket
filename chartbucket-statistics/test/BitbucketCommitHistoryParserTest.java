import data_providers.bitbucket.BitbucketCommitHistoryParser;
import data_structures.Author;
import data_structures.Commit;
import data_structures.Repository;
import junit.framework.Assert;
import org.junit.Test;
import play.libs.Json;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BitbucketCommitHistoryParserTest {
    private static final String TEST_JSON =
            "{" +
            " \"pagelen\": 30," +
            "  \"values\": [" +
            "    {" +
            "      \"hash\": \"be3706858026cac5cb5a1fa737756422836237b0\"," +
            "      \"repository\": {" +
            "        \"uuid\": \"{fc311a3f-a1a2-4f55-a73e-a9745366b4f4}\"," +
            "        \"full_name\": \"chartbucket/chartbucket\"," +
            "        \"name\": \"Chartbucket\"" +
            "      }," +
            "      \"author\": {" +
            "        \"raw\": \"Test User <test@user.com>\"," +
            "        \"user\": {" +
            "          \"username\": \"TestUser\"," +
            "          \"display_name\": \"Test User\"," +
            "          \"uuid\": \"{2aa7b69b-dd78-4fed-b284-5c0aaebb41ef}\"" +
            "        }" +
            "      }," +
            "      \"date\": \"2014-12-17T13:20:48+00:00\"," +
            "      \"message\": \"test\"" +
            "    }" +
            "  ]" +
            "}";

    private static final String NO_USER_INFO_JSON =
            "{" +
            " \"pagelen\": 30," +
            "  \"values\": [" +
            "    {" +
            "      \"hash\": \"be3706858026cac5cb5a1fa737756422836237b0\"," +
            "      \"repository\": {" +
            "        \"uuid\": \"{fc311a3f-a1a2-4f55-a73e-a9745366b4f4}\"," +
            "        \"full_name\": \"chartbucket/chartbucket\"," +
            "        \"name\": \"Chartbucket\"" +
            "      }," +
            "      \"author\": {" +
            "        \"raw\": \"Test User <test@user.com>\"" +
            "      }," +
            "      \"date\": \"2014-12-17T13:20:48+00:00\"," +
            "      \"message\": \"test\"" +
            "    }" +
            "  ]" +
            "}";
    @Test
    public void positiveTest() {
        List<Commit> expectedResult = new ArrayList<>();
        expectedResult.add(new Commit(
                new Author("Test User <test@user.com>", "TestUser"),
                new Repository("chartbucket/chartbucket"),
                LocalDateTime.of(2014, 12, 17, 13, 20, 48),
                "test"
        ));

        BitbucketCommitHistoryParser parser = new BitbucketCommitHistoryParser();
        List<Commit> actualResult = parser.parse(Json.parse(TEST_JSON));

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void noUserInfo() {
        List<Commit> expectedResult = new ArrayList<>();
        expectedResult.add(new Commit(
                new Author("Test User <test@user.com>", ""),
                new Repository("chartbucket/chartbucket"),
                LocalDateTime.of(2014, 12, 17, 13, 20, 48),
                "test"
        ));

        BitbucketCommitHistoryParser parser = new BitbucketCommitHistoryParser();
        List<Commit> actualResult = parser.parse(Json.parse(NO_USER_INFO_JSON));

        Assert.assertEquals(expectedResult, actualResult);
    }
}
