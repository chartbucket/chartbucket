import data_providers.AuthInfo;
import data_providers.RepositoryInfo;
import data_providers.bitbucket.BitbucketCommitHistoryProvider;
import data_providers.bitbucket.BitbucketConnector;
import data_providers.bitbucket.BitbucketConnector.*;
import data_structures.Author;
import data_structures.Commit;
import data_structures.Repository;
import org.junit.Assert;
import org.junit.Test;
import play.libs.F;
import play.libs.Json;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HistoryProviderTest {
    private static final String SINGLE_PAGE_JSON =
            "{" +
            " \"pagelen\": 30," +
            " \"values\": [" +
            "     {" +
            "      \"hash\": \"be3706858026cac5cb5a1fa737756422836237b0\"," +
            "      \"repository\": {" +
            "        \"uuid\": \"{fc311a3f-a1a2-4f55-a73e-a9745366b4f4}\"," +
            "        \"full_name\": \"chartbucket/chartbucket\"," +
            "        \"name\": \"Chartbucket\"" +
            "      }," +
            "      \"author\": {" +
            "        \"raw\": \"Test User <test@user.com>\"," +
            "        \"user\": {" +
            "          \"username\": \"TestUser\"," +
            "          \"display_name\": \"Test User\"," +
            "          \"uuid\": \"{2aa7b69b-dd78-4fed-b284-5c0aaebb41ef}\"" +
            "        }" +
            "      }," +
            "      \"date\": \"2014-12-17T13:20:48+00:00\"," +
            "      \"message\": \"test1\"" +
            "    }" +
            "  ]" +
            "}";

    private static final String WITH_NEXT_PAGE_JSON =
            "{" +
            " \"pagelen\": 30," +
            " \"values\": [" +
            "     {" +
            "      \"hash\": \"be3706858026cac5cb5a1fa737756422836237b0\"," +
            "      \"repository\": {" +
            "        \"uuid\": \"{fc311a3f-a1a2-4f55-a73e-a9745366b4f4}\"," +
            "        \"full_name\": \"chartbucket/chartbucket\"," +
            "        \"name\": \"Chartbucket\"" +
            "      }," +
            "      \"author\": {" +
            "        \"raw\": \"Test User <test@user.com>\"," +
            "        \"user\": {" +
            "          \"username\": \"TestUser\"," +
            "          \"display_name\": \"Test User\"," +
            "          \"uuid\": \"{2aa7b69b-dd78-4fed-b284-5c0aaebb41ef}\"" +
            "        }" +
            "      }," +
            "      \"date\": \"2014-12-17T13:20:48+00:00\"," +
            "      \"message\": \"test2\"" +
            "    }" +
            "  ]," +
            "  \"next\": \"next\"" +
            "}";

    private static final String EMPTY_JSON =
            "{" +
            " \"pagelen\": 30," +
            " \"values\": []" +
            "}";

    @Test
    public void singlePage() {
        BitbucketConnector fakeConnector = mock(BitbucketConnector.class);
        when(fakeConnector.retreiveAllCommitsHistory("test", "test", null, 1))
                .thenReturn(F.Promise.promise(() -> new CommitHistoryPage(Json.parse(SINGLE_PAGE_JSON))));

        List<Commit> expectedResult = new ArrayList<>();
        expectedResult.add(new Commit(
                new Author("Test User <test@user.com>", "TestUser"),
                new Repository("chartbucket/chartbucket"),
                LocalDateTime.of(2014, 12, 17, 13, 20, 48),
                "test1"
        ));

        RepositoryInfo repo = new RepositoryInfo("test", "test");
        AuthInfo auth = new AuthInfo(null);

        List<Commit> actualResult = new BitbucketCommitHistoryProvider(fakeConnector)
                .retrieveHistory(repo, auth).get(1000);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void multiplePages() {
        BitbucketConnector fakeConnector = mock(BitbucketConnector.class);
        when(fakeConnector.retreiveAllCommitsHistory("test", "test", null, 1))
                .thenReturn(F.Promise.promise(() -> new CommitHistoryPage(Json.parse(WITH_NEXT_PAGE_JSON))));

        when(fakeConnector.retreiveAllCommitsHistory("test", "test", null, 2))
                .thenReturn(F.Promise.promise(() -> new CommitHistoryPage(Json.parse(SINGLE_PAGE_JSON))));

        List<Commit> expectedResult = new ArrayList<>();
        expectedResult.add(new Commit(
                new Author("Test User <test@user.com>", "TestUser"),
                new Repository("chartbucket/chartbucket"),
                LocalDateTime.of(2014, 12, 17, 13, 20, 48),
                "test2"
        ));

        expectedResult.add(new Commit(
                new Author("Test User <test@user.com>", "TestUser"),
                new Repository("chartbucket/chartbucket"),
                LocalDateTime.of(2014, 12, 17, 13, 20, 48),
                "test1"
        ));

        RepositoryInfo repo = new RepositoryInfo("test", "test");
        AuthInfo auth = new AuthInfo(null);

        List<Commit> actualResult = new BitbucketCommitHistoryProvider(fakeConnector)
                .retrieveHistory(repo, auth).get(1000);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void emptyList() {
        BitbucketConnector fakeConnector = mock(BitbucketConnector.class);
        when(fakeConnector.retreiveAllCommitsHistory("test", "test", null, 1))
                .thenReturn(F.Promise.promise(() -> new CommitHistoryPage(Json.parse(EMPTY_JSON))));

        RepositoryInfo repo = new RepositoryInfo("test", "test");
        AuthInfo auth = new AuthInfo(null);

        List<Commit> actualResult = new BitbucketCommitHistoryProvider(fakeConnector)
                .retrieveHistory(repo, auth).get(1000);

        Assert.assertTrue(actualResult.isEmpty());
    }
}
