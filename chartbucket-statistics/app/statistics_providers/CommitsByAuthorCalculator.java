package statistics_providers;


import data_structures.Commit;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommitsByAuthorCalculator {
    public Map<String, Integer> calculate(List<Commit> commits) {
        return commits.stream().collect(Collectors.groupingBy(c -> c.getAuthor().getDisplayName(), Collectors.summingInt(c -> 1)));
    }
}
