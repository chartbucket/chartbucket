package statistics_providers;

import data_structures.Commit;

import java.time.LocalDateTime;
import java.util.List;

public class CommitCountCalculator {
    public long commitsCount(List<Commit> commits, LocalDateTime startTime, LocalDateTime stopTime) {
        return commits.stream()
                .filter(commit -> commit.getTime().isAfter(startTime) && commit.getTime().isBefore(stopTime))
                .count();
    }
}
