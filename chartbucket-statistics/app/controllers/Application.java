package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import data_providers.AuthInfo;
import data_providers.CommitHistoryProvider;
import data_providers.RepositoryInfo;
import data_providers.bitbucket.BitbucketCommitHistoryProvider;
import data_providers.bitbucket.BitbucketConnector;
import exceptions.AccessDeniedException;
import exceptions.RepositoryNotFoundException;
import play.*;
import play.libs.F.*;
import play.libs.Json;
import play.libs.oauth.OAuth;
import play.mvc.*;
import statistics_providers.CommitCountCalculator;
import statistics_providers.CommitsByAuthorCalculator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;

public class Application extends Controller {
    private static final OAuth.ConsumerKey CONSUMER_KEY = new OAuth.ConsumerKey(
            Play.application().configuration().getString("bitbucket_consumer_key"),
            Play.application().configuration().getString("bitbucket_consumer_secret"));

    public static Result index() {
        return ok("Chartbucket statistics service");
    }

    public static Promise<Result> history(String repoOwner, String repoName) {
        CommitHistoryProvider historyProvider = new BitbucketCommitHistoryProvider(new BitbucketConnector(CONSUMER_KEY));
        return historyProvider
                .retrieveHistory(new RepositoryInfo(repoOwner, repoName), new AuthInfo(null))
                .map(history -> ok(Json.toJson(history)));
    }

    public static Promise<Result> commitsCount(String repoOwner,
                                               String repoName,
                                               String startDateTime,
                                               String stopDateTime) {
        CommitHistoryProvider historyProvider = new BitbucketCommitHistoryProvider(new BitbucketConnector(CONSUMER_KEY));
        return historyProvider
                .retrieveHistory(new RepositoryInfo(repoOwner, repoName), new AuthInfo(getSessionTokenPair()))
                .map(history -> {
                    CommitCountCalculator commitCountCalculator = new CommitCountCalculator();
                    LocalDateTime start = LocalDateTime.parse(startDateTime, DateTimeFormatter.ISO_DATE_TIME);
                    LocalDateTime stop = LocalDateTime.parse(stopDateTime, DateTimeFormatter.ISO_DATE_TIME);
                    return commitCountCalculator.commitsCount(history, start, stop);
                })
                .map(count -> (Result) ok(Json.newObject().put("count", count.toString())))
                .recover(ex -> {
                    if (ex instanceof DateTimeParseException) {
                        return badRequest(Json.newObject()
                                .put("error", "Cannot parse date")
                                .put("details", ex.getMessage()));
                    }
                    if (ex instanceof AccessDeniedException) {
                        return status(401, Json.newObject()
                                .put("error", "Unauthorized")
                                .put("details", ex.getMessage()));
                    } else if (ex instanceof RepositoryNotFoundException) {
                        return status(404, Json.newObject()
                                .put("error", "not found")
                                .put("details", ex.getMessage()));
                    } else {
                        return internalServerError(Json.newObject()
                                .put("error", "Something bad and unexpected happened... ")
                                .put("details", ex.getMessage()));
                    }
                });
    }

    public static Promise<Result> contributors(String repoOwner, String repoName) {
        CommitHistoryProvider historyProvider = new BitbucketCommitHistoryProvider(new BitbucketConnector(CONSUMER_KEY));
        return historyProvider
                .retrieveHistory(new RepositoryInfo(repoOwner, repoName), new AuthInfo(getSessionTokenPair()))
                .map(history -> new CommitsByAuthorCalculator().calculate(history))
                .map(r -> {
                    ArrayNode array = Json.newObject().putArray("values");

                    for (Map.Entry<String, Integer> e : r.entrySet()) {
                        array.add(Json.newObject().put("name", e.getKey()).put("count", e.getValue()));
                    }

                    return (Result) ok(array);
                })
                .recover(ex -> {
                    if (ex instanceof AccessDeniedException) {
                        return status(401, Json.newObject()
                                .put("error", "Unauthorized")
                                .put("details", ex.getMessage()));
                    } else if (ex instanceof RepositoryNotFoundException) {
                        return status(404, Json.newObject()
                                .put("error", "not found")
                                .put("details", ex.getMessage()));
                    } else {
                        return internalServerError(Json.newObject()
                                .put("error", "Something bad and unexpected happened... ")
                                .put("details", ex.getMessage()));
                    }
                });
    }

    private static OAuth.RequestToken getSessionTokenPair() {
        if (session().containsKey("token")) {
            return new OAuth.RequestToken(session("token"), session("secret"));
        }
        else {
            return null;
        }
    }
}
