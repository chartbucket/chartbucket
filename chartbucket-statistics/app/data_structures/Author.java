package data_structures;


public class Author {
    private String _displayName;
    private String _userName;

    public Author(String displayName, String userName) {
        _displayName = displayName;
        _userName = userName;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public String getUserName() {
        return _userName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Author))
            return false;

        Author that = (Author) obj;

        return this._displayName.equals(that._displayName) &&
               this._userName.equals(that._userName);
    }
}
