package data_structures;


import java.time.LocalDateTime;

public class Commit {
    private Author _author;
    private Repository _repository;
    private LocalDateTime _time;
    private String _message;

    public Commit(Author author, Repository repository, LocalDateTime time, String message) {
        _author = author;
        _repository = repository;
        _time = time;
        _message = message;
    }

    public Author getAuthor() {
        return _author;
    }

    public Repository getRepository() {
        return _repository;
    }

    public LocalDateTime getTime() {
        return _time;
    }

    public String getMessage() {
        return _message;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Commit))
            return false;

        Commit that = (Commit) obj;

        return this._author.equals(that._author) &&
               this._message.equals(that._message) &&
               this._repository.equals(that._repository) &&
               this._time.equals(that._time);
    }
}
