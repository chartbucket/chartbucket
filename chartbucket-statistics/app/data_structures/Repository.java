package data_structures;


public class Repository {
    private String _name;

    public Repository(String name) {
        _name = name;
    }

    public String getName() {
        return _name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Repository))
            return false;

        Repository that = (Repository) obj;

        return this._name.equals(that._name);
    }
}
