package data_providers;


import data_structures.Commit;
import play.libs.F.*;

import java.util.List;

public abstract class CommitHistoryProvider {
    public abstract Promise<List<Commit>> retrieveHistory(RepositoryInfo repo, AuthInfo authInfo);
}
