package data_providers;


public class RepositoryInfo {
    private String _owner;
    private String _name;

    public RepositoryInfo(String owner, String name) {
        _owner = owner;
        _name = name;
    }

    public String getOwner() {
        return _owner;
    }

    public String getName() {
        return _name;
    }
}
