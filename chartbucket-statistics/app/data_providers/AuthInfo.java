package data_providers;


import play.libs.oauth.OAuth.*;

public class AuthInfo {
    private final RequestToken _oauth1Token;

    public AuthInfo() {
        _oauth1Token = null;
    }

    public AuthInfo(RequestToken oauth1Token) {
        _oauth1Token = oauth1Token;
    }

    public RequestToken getOauth1Token() {
        return _oauth1Token;
    }
}
