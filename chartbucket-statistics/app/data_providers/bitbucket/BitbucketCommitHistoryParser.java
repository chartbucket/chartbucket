package data_providers.bitbucket;


import com.fasterxml.jackson.databind.JsonNode;
import data_structures.Author;
import data_structures.Commit;
import data_structures.Repository;
import play.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BitbucketCommitHistoryParser {
    public List<Commit> parse(JsonNode node) {
        List<Commit> result = new ArrayList<>();
        JsonNode valuesNode = node.get("values");

        for (int i = 0; i < valuesNode.size(); i++) {
            JsonNode commitNode = valuesNode.get(i);
            Commit commit = parseCommit(commitNode);
            result.add(commit);
        }

        return result;
    }

    private Commit parseCommit(JsonNode node) {
        Repository repository = parseRepo(node.get("repository"));
        Author author = parseAuthor(node.get("author"));
        LocalDateTime time = LocalDateTime.parse(node.get("date").asText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String message = node.get("message").asText();

        return new Commit(author, repository, time, message);
    }

    private Repository parseRepo(JsonNode node) {
        String name = node.get("full_name").asText();

        return new Repository(name);
    }

    private Author parseAuthor(JsonNode node) {
        Logger.debug(node.toString());
        String raw = node.get("raw").asText();

        JsonNode userNode = node.get("user");
        String userName = userNode != null ? userNode.get("username").asText() : "";

        return new Author(raw, userName);
    }
}
