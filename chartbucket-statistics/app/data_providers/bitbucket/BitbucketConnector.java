package data_providers.bitbucket;


import com.fasterxml.jackson.databind.JsonNode;
import exceptions.AccessDeniedException;
import exceptions.DataProviderException;
import exceptions.RepositoryNotFoundException;
import play.libs.oauth.OAuth.*;
import play.libs.ws.*;
import play.libs.F.Promise;

public class BitbucketConnector {

    public static class CommitHistoryPage {
        JsonNode _currPage;
        boolean _last;

        public CommitHistoryPage(JsonNode currPage) {
            _currPage = currPage;
            _last = !currPage.has("next");
        }

        public JsonNode getCurrPage() {
            return _currPage;
        }

        public boolean isLast() {
            return _last;
        }
    }

    private final ConsumerKey _consumerKey;

    public BitbucketConnector(ConsumerKey consumerKey) {
        _consumerKey = consumerKey;
    }

    public Promise<CommitHistoryPage> retreiveAllCommitsHistory(String repoOwner, String repoName, RequestToken requestToken) {
        return retreiveAllCommitsHistory(repoOwner, repoName, requestToken, 1);
    }

    public Promise<CommitHistoryPage> retreiveAllCommitsHistory(String repoOwner,
                                                                String repoName,
                                                                RequestToken requestToken,
                                                                int page) {
        WSRequestHolder request =
                WS.url("https://bitbucket.org/api/2.0/repositories/" + repoOwner + "/" + repoName + "/commits")
                  .setQueryParameter("pagelen", "100");

        if (requestToken != null) {
            request = request.sign(new OAuthCalculator(_consumerKey, requestToken));
        }

        if (page > 1) {
            request = request.setQueryParameter("page", String.valueOf(page));
        }

        return request.get().map(response -> {
            if (response.getStatus() == 401) {
                throw new AccessDeniedException(String.format("Access to repository %s/%s denied", repoOwner, repoName));
            }
            else if (response.getStatus() == 404) {
                throw new RepositoryNotFoundException(String.format("Repository %s/%s not found", repoOwner, repoName));
            }
            else if (response.getStatus() != 200) {
                throw new DataProviderException("Failed to retrieve data from remote repository");
            }

            return new CommitHistoryPage(response.asJson());
        });
    }
}
