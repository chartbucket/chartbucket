package data_providers.bitbucket;


import data_providers.AuthInfo;
import data_providers.CommitHistoryProvider;
import data_providers.RepositoryInfo;
import data_structures.Commit;
import play.libs.F.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BitbucketCommitHistoryProvider extends CommitHistoryProvider {
    private static final int TIMEOUT_SEC = 10;

    private final BitbucketConnector _bitbucketConnector;
    private final BitbucketCommitHistoryParser _historyParser;

    public BitbucketCommitHistoryProvider(BitbucketConnector bitbucketConnector) {
        _bitbucketConnector = bitbucketConnector;
        _historyParser = new BitbucketCommitHistoryParser();
    }

    @Override
    public Promise<List<Commit>> retrieveHistory(RepositoryInfo repo, AuthInfo authInfo) {
        return Promise.promise(() -> pageAggregator(repo, authInfo));
    }

    private List<Commit> pageAggregator(RepositoryInfo repo, AuthInfo auth) {
        List<Commit> result = new ArrayList<>();

        BitbucketConnector.CommitHistoryPage firstPage = _bitbucketConnector
                .retreiveAllCommitsHistory(repo.getOwner(), repo.getName(), auth.getOauth1Token(), 1)
                .get(TIMEOUT_SEC, TimeUnit.SECONDS);

        result.addAll(_historyParser.parse(firstPage.getCurrPage()));

        boolean lastPage = firstPage.isLast();
        for (int pageNum = 2; !lastPage; ++ pageNum) {
            BitbucketConnector.CommitHistoryPage currPage  = _bitbucketConnector
                    .retreiveAllCommitsHistory(repo.getOwner(), repo.getName(), auth.getOauth1Token(), pageNum)
                    .get(TIMEOUT_SEC, TimeUnit.SECONDS);

            result.addAll(_historyParser.parse(currPage.getCurrPage()));
            lastPage = currPage.isLast();
        }

        return result;
    }
}
