package controllers;

import com.google.common.base.Strings;
import play.Play;
import play.libs.oauth.OAuth;
import play.libs.oauth.OAuth.RequestToken;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {
    private static final OAuth.ConsumerKey Key = new OAuth.ConsumerKey(
            Play.application().configuration().getString("bitbucket_consumer_key"),
            Play.application().configuration().getString("bitbucket_consumer_secret"));

    private static final OAuth BitbucketSerivce = new OAuth(
            new OAuth.ServiceInfo(
                    Play.application().configuration().getString("bitbucket_request_token_url"),
                    Play.application().configuration().getString("bitbucket_access_token_url"),
                    Play.application().configuration().getString("bitbucket_auth_url"),
                    Key),
            true);

    public static Result index() {
        return ok("Hello");
    }

    public static Result bitbucketAuth(String redirectUrl) {
        String verifier = request().getQueryString("oauth_verifier");
        if (Strings.isNullOrEmpty(verifier)) {
            String url = routes.Application.bitbucketAuth(redirectUrl).absoluteURL(request());
            RequestToken requestToken = BitbucketSerivce.retrieveRequestToken(url);
            saveSessionTokenPair(requestToken);
            return redirect(BitbucketSerivce.redirectUrl(requestToken.token));
        } else {
            RequestToken requestToken = getSessionTokenPair();
            RequestToken accessToken = BitbucketSerivce.retrieveAccessToken(requestToken, verifier);
            saveSessionTokenPair(accessToken);
            return redirect(redirectUrl);
        }
    }

    private static void saveSessionTokenPair(RequestToken requestToken) {
        session("token", requestToken.token);
        session("secret", requestToken.secret);
    }

    private static RequestToken getSessionTokenPair() {
        if (session().containsKey("token")) {
            return new RequestToken(session("token"), session("secret"));
        }
        else {
            return null;
        }
    }
}
